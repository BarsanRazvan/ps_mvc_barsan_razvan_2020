﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoshopMVC.Models;
using BAL.Interfaces;
using BAL.Models;
using BAL.Services;
using DAL.Entities;
using javax.jws;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AutoshopMVC.Controllers
{

    public class AppointmentController : Controller
    {
        private IAppointmentService appointmentService;
        private UserManager<User> _userManager;
        BigAppointmentModel model;

        public AppointmentController(UserManager<User> userManager)
        {
            appointmentService = new AppointmentService();
            _userManager = userManager;
            model = new BigAppointmentModel();
        }

        [Authorize]
        public IActionResult Index()
        {
            return View(model);
        }
        
        [Authorize]
        [HttpPost]
        public IActionResult MakeAppointment(BigAppointmentModel bigAppointment)
        {
            bigAppointment.appointmentModel.CreationUser = _userManager.GetUserAsync(User).Id;
            appointmentService.MakeNewAppointment(bigAppointment.appointmentModel);
            
            return View("Index");
        }

        [Authorize]
        [HttpGet]
        public IActionResult RefreshAppointments(BigAppointmentModel model)
        {
            model.appointmentsPerDay =  appointmentService.GenerateReports(model.startDate.Date, model.startDate.Date.AddDays(1));
            return View("Index", model);
        }

        [Authorize]
        public IActionResult FinalizeAppointment(int id)
        {
                AppointmentModel appointment = appointmentService.GetAppointment(id);
                appointmentService.FinishAppointment(appointment);
                return View("Index", model);
        }

        [Authorize]
        [HttpGet]
        public IActionResult GenerateReports(BigAppointmentModel model)
        {
            model.generatedAppointmentReports = appointmentService.GenerateReports(model.startDate.Date, model.endDate.Date.AddDays(1));
            return View("Index", model);
        }
    }
}