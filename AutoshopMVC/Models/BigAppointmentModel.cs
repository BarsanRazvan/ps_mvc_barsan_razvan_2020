﻿using BAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutoshopMVC.Models
{
    public class BigAppointmentModel
    {
        public AppointmentModel appointmentModel { get; set; }
        public IEnumerable<BAL.Models.AppointmentModel> appointmentsPerDay { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public IEnumerable<BAL.Models.AppointmentModel> generatedAppointmentReports { get; set; }

        public BigAppointmentModel()
        {
            appointmentsPerDay = new List<AppointmentModel>();
            generatedAppointmentReports = new List<AppointmentModel>();
            startDate = DateTime.Now;
            endDate = DateTime.Now;
        }
    }
}
