﻿using BAL.Interfaces;
using BAL.Models;
using BAL.Utilities;
using DAL.Entities;
using DAL.Interfaces;
using DAL.UnitsOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BAL.Services
{
    public class AppointmentService : IAppointmentService
    {
        IUnitOfWork unitOfWork;

        public AppointmentService()
        {
            unitOfWork = new UnitOfWork();
        }

        public AppointmentModel GetAppointment(int id)
        {
            Appointment appointment = unitOfWork.Appointments.GetByID(id);
            if (appointment != null)
            {
                return ModelMapper.MapValue<Appointment, AppointmentModel>(appointment);
            }
            return null;
        }

        public int FinishAppointment(AppointmentModel appointment)
        {
            if (appointment.ID < 1 || appointment.DateOfAppointment == null)
            {
                return 1;
            }

            appointment.StatusOfAppointment = "Finished";
            unitOfWork.Appointments.Update(ModelMapper.MapValue<AppointmentModel, Appointment>(appointment));
            unitOfWork.Save();
            return 0;
        }

        public IEnumerable<AppointmentModel> GenerateReports(DateTime startDate, DateTime endDate)
        {
            return unitOfWork.Appointments.Get(appointment => DateTime.Compare((DateTime)appointment.DateOfAppointment, startDate) >= 0 &&
                                                            DateTime.Compare((DateTime)appointment.DateOfAppointment, endDate) <= 0).Select(
                                                            t => ModelMapper.MapValue<Appointment, AppointmentModel>(t)).ToList();
        }

        public AppointmentModel GetAppointmentByDate(DateTime date)
        {
            Appointment currentAppointment = unitOfWork.Appointments.Get(appointment => DateTime.Compare((DateTime)appointment.DateOfAppointment, date) == 0).FirstOrDefault(); ;
            if (currentAppointment != null)
            {
                return ModelMapper.MapValue<Appointment, AppointmentModel>(currentAppointment);
            }

            return null;
        }

        public int MakeNewAppointment(AppointmentModel appointment)
        {
            appointment.CreationDate = DateTime.Now;
            appointment.StatusOfAppointment = "Not Finished";
            unitOfWork.Appointments.Insert(ModelMapper.MapValue<AppointmentModel, Appointment>(appointment));
            unitOfWork.Save();
            Console.WriteLine("Hehe");
            return 0;
        }
    }
}
