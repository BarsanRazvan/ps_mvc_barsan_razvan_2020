﻿using BAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Interfaces
{
    public interface IAppointmentService
    {
        AppointmentModel GetAppointment(int id);
        int MakeNewAppointment(AppointmentModel appointment);
        int FinishAppointment(AppointmentModel appointment);
        AppointmentModel GetAppointmentByDate(DateTime date);
        IEnumerable<AppointmentModel> GenerateReports(DateTime startDate, DateTime endDate);
    }
}
