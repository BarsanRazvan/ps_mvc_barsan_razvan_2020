﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Models
{
    public class UserModel
    {
        public int ID { get; set; }
        public string FullName { get; set; }
        public string PhoneNo { get; set; }
        public DateTime DateOfEnrollment { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string UserPassword { get; set; }

        public UserModel()
        {

        }
    }
}
