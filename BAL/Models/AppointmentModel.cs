﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BAL.Models
{
    public class AppointmentModel
    {
        public int ID { get; set; }
        [DisplayName("Date")]
        public DateTime DateOfAppointment { get; set; }
        [DisplayName("First Name")]
        public string ClientName { get; set; }
        [DisplayName("Phone Number")]
        public string ClientPhoneNo { get; set; }
        [DisplayName("Car Model")]
        public string CarModel { get; set; }
        [DisplayName("Description")]
        public string ProblemDescription { get; set; }
        public int CreationUser { get; set; }
        public DateTime CreationDate { get; set; }
        public string StatusOfAppointment { get; set; }

        public AppointmentModel()
        {

        }
    }
}
