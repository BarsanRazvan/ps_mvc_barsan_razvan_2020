﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Entities
{
    public class User : IdentityUser<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            this.Appointments = new HashSet<Appointment>();
        }

        public string FullName { get; set; }
        public System.DateTime DateOfEnrollment { get; set; }
        public string PhoneNo { get; set; }

        [ForeignKey("CreationUser")]
        public virtual ICollection<Appointment> Appointments { get; set; }
    }
}
