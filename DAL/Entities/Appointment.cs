﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Appointment
    {
        public int ID { get; set; }
        public System.DateTime DateOfAppointment { get; set; }
        public string ClientName { get; set; }
        public string ClientPhoneNo { get; set; }
        public string CarModel { get; set; }
        public string ProblemDescription { get; set; }
        public System.DateTime CreationDate { get; set; }
        public string StatusOfAppointment { get; set; }

        public int CreationUser { get; set; }
    }
}
